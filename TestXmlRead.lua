local LoadXml = require "LoadXml"
require "_TableWriter"
local Build = require "_BuildDataFromXml"

local xmlData, errors = LoadXml.LoadXml("newglspecs/wgl.xml")


local data = Build.BuildData(xmlData, nil, "wgl", "WGL_", "wgl")

local hFile = io.open("test3.lua", "w")
WriteTable(hFile, data)
hFile:close()
