
require "XmlWriter"

local specFilename, outputFilename, name, specVersion = ...

if(not specVersion) then
	print("Not enough arguments:");
	print("Usage:");
	print("\tspecFilename\tName of the Lua spec file.");
	print("\toutputFile\tName of the XML file to write.");
	print("\tname\t\tInternal name of the specification.");
	print("\tspecVersion\tThe version of the specification.");

	return -1;
end

local specFile = dofile(specFilename);

local xmlFile = XmlWriter.XmlWriter(outputFilename);

xmlFile:PushElement("specification", "http://www.opengl.org/registry/");
xmlFile:AddAttribute("name", name);
xmlFile:AddAttribute("specversion", specVersion);
xmlFile:AddAttribute("fileversion", "0.0.1");

do
	xmlFile:PushElement("typemap");
	
	for key, value in pairs(specFile.typemap) do
		xmlFile:PushElement("type-def");
		xmlFile:AddAttribute("typename", key);
		xmlFile:AddAttribute("C-lang", value);
		xmlFile:PopElement();
	end

	xmlFile:PopElement();
end

do
	xmlFile:PushElement("extensions");
	
	for i, value in ipairs(specFile.extensions) do
		xmlFile:PushElement("ext");
		xmlFile:AddAttribute("name", value);
		xmlFile:PopElement();
	end
	
	xmlFile:PopElement();
end

do
	xmlFile:PushElement("enumerations");
	
	for i, enum in ipairs(specFile.enumerations) do
		xmlFile:PushElement("enum");
		xmlFile:AddAttribute("name", enum.name);
		if(enum.copy) then
			xmlFile:AddAttribute("ref", enum.value);
		else
			xmlFile:AddAttribute("value", enum.value);
		end
		if(enum.version) then xmlFile:AddAttribute("version", enum.version); end
		if(enum.deprecated) then xmlFile:AddAttribute("deprecated", enum.deprecated); end
		if(enum.removed) then xmlFile:AddAttribute("removed", enum.removed); end
		
		if(enum.extensions) then
			for i, ext in ipairs(enum.extensions) do
				xmlFile:PushElement("ext");
					xmlFile:AddAttribute("name", ext);
				xmlFile:PopElement();
			end
		end
		
		xmlFile:PopElement();
	end
	
	xmlFile:PopElement();
end

do
	xmlFile:PushElement("functions");
	
	if(specFile.funcData.passthru) then
		xmlFile:PushElement("passthru");
		xmlFile:AddCDataText(table.concat(specFile.funcData.passthru, "\n"));
		xmlFile:PopElement();
	end
	
	xmlFile:PushElement("property-defs");
	for name, array in pairs(specFile.funcData.properties) do
		xmlFile:PushElement("property");
		xmlFile:AddAttribute("name", name);
		if((#array == 0) or (array[1] == "*")) then
			xmlFile:AddAttribute("any", "true");
		else
			for i, property in ipairs(array) do
				xmlFile:PushElement("value");
				xmlFile:AddText(property);
				xmlFile:PopElement();
			end
		end
		xmlFile:PopElement();
	end
	xmlFile:PopElement();
	
	xmlFile:PushElement("function-defs");
	for i, func in ipairs(specFile.funcData.functions) do
		xmlFile:PushElement("function");
		xmlFile:AddAttribute("name", func.name);
		xmlFile:AddAttribute("return", func["return"]);
		
		for propName, propValue in pairs(func) do
			if(propName ~= "name" and
				propName ~= "return" and
				propName ~= "params" and
				propName ~= "param") then
				xmlFile:AddAttribute(propName, propValue);
			end
		end
		
		for i, param in ipairs(func.params) do
			xmlFile:PushElement("param");
			xmlFile:AddAttribute("name", param.name);
			xmlFile:AddAttribute("kind", param.kind);
			xmlFile:AddAttribute("type", param.type);
			local value;
			if(param.input) then value = "true" else value = "false" end
			xmlFile:AddAttribute("input", value);
			if(param.other) then xmlFile:AddAttribute("compute", param.other) end
			xmlFile:PopElement();
		end
		
		xmlFile:PopElement();
	end
	xmlFile:PopElement();
	
	xmlFile:PopElement();
end

xmlFile:PopElement();

xmlFile:Close();

