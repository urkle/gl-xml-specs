
The raw spec.lua files are formatted as follows:

- `funcData`: A table containing:
	- `passthru`: An array of C definitions that should be defined before anything else. They should be defined in the given order.
	- `functions`: An array of function definitions for all functions. Each function has:
		- `name`: The base name of the function.
		- `return_ctype`: The C-language return type.
		- `parameters`: An array of function parameters, in order. Each parameter entry has:
			- `name`: The name of the parameter.
			- `ctype`: The C-language type of the parameter.
		- **Designation**: Defines what extension(s) and version(s) this function appears in.
- `enumerators`: An array of all enumerators. Eacn enumerator has:
	- `name`: The base name of the enumerator.
	- `value`: The value of the enumerator. These are never links to other enumerators.
	- **Designation**: Defines what extension(s) and version(s) this enumerator appears in.
- `versions`: An array of version strings. These are numerical: "1.0", "1.3", etc.
- `extensions`: An array of extension string names.


Designation:

The "designation" is not a specific member; it is a collection of members that explain where the specified enum/function appears.

There are two designators, at least one of which must appear in each func/enum definition. The designators are:

- `extensions`: An array of extension string names for the definition.
- `core`: An array of versions to which this definition belongs. The versions are listed from oldest version to most recent. Each version is an array, where index [1] is the version number (as a string) and index [2] is either "core" or "compatibility". The latter means that it is exposed in core GL or just the compatibility profile.
    The one that affects the current definition is the last one that is still less than the current version.
