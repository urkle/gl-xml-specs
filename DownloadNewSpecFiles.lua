local io = require("io")
local http = require("socket.http")
local ltn12 = require("ltn12")

local files = {
	"enum.spec",
	"enumext.spec",
	"gl.spec",
	"gl.tm",
	
	"glxenum.spec",
	"glxenumext.spec",
	"glx.spec",
	"glxext.spec",
	"glx.tm",

	"wglenum.spec",
	"wglenumext.spec",
	"wgl.spec",
	"wglext.spec",
	"wgl.tm",
}

local urlPrefix = [=[http://www.opengl.org/registry/api/]=]
local destPrefix = [=[glspecs\]=]

for i, specFile in ipairs(files) do
	print("Downloading " .. specFile);
	local hOutFile = assert(io.open(destPrefix .. specFile, "wb"));
	http.request {url = urlPrefix .. specFile, sink = ltn12.sink.file(hOutFile)}
end


